-- This script retrieves the highest priority diagnosis for each hospital admission

SELECT list.HADM_ID, map.L1_CAT
    FROM `fyp-mimic.fyp_extracts.diagnoses_icd` list
    LEFT JOIN `fyp-mimic.fyp_external.diagnosis_mapping` map ON list.ICD9_CODE=map.ICD9_CODE
    WHERE list.SEQ_NUM = 1