-- This scipt extracts the OASIS score and probability of mortality for each ICU stay

SELECT 
    icustay_id AS ICUSTAY_ID,
    icustay_expire_flag AS ICUSTAY_EXPIRE_FLAG,
    oasis AS OASIS_SCORE,
    oasis_PROB AS OASIS_PROB_OF_IN_HOSP_MORTALITY
    FROM `fyp-mimic.fyp_extracts.oasis`