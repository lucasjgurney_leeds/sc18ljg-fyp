-- This script obtains a list of all ICU stays and their corresponding hospital admissions

SELECT DISTINCT HADM_ID, ICUSTAY_ID
    FROM `fyp-mimic.fyp_extracts.icustays`
    ORDER BY ICUSTAY_ID