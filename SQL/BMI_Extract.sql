-- This script obtains the BMI class for each patient.

SELECT 
        *,
        CASE 
            WHEN BMI < 18.5 THEN "Underweight"
            WHEN BMI < 25.0 THEN "Healthy"
            WHEN BMI < 30.0 THEN "Overweight"
            WHEN BMI >= 30.0 THEN "Obese"
            ELSE "Unknown"
        END AS BMI_CATEGORY
    FROM (
        SELECT 
                icustay_id AS ICUSTAY_ID,
                weight_first AS WEIGHT,
                height_first AS HEIGHT,
                weight_first / NULLIF(POWER(height_first / 100, 2), 0) AS BMI
            FROM `fyp-mimic.fyp_extracts.heightweight`
    )