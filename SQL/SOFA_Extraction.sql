-- This script extracts the SOFA renal function score for each patient.

SELECT 
        icustay_id AS ICUSTAY_ID,
        renal AS SOFA_RENAL_SCORE
    FROM `fyp-mimic.fyp_extracts.sofa`