-- Script should be copied into BigQuery and executed directly in there.
-- Comment out tables that do not need extracting again.

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.admissions`;
CREATE TABLE `fyp-mimic.fyp_extracts.admissions` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.admissions`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.patients`;
CREATE TABLE `fyp-mimic.fyp_extracts.patients` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.patients`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.transfers`;
CREATE TABLE `fyp-mimic.fyp_extracts.transfers` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.transfers`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.diagnoses_icd`;
CREATE TABLE `fyp-mimic.fyp_extracts.diagnoses_icd` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.diagnoses_icd`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.d_icd_diagnoses`;
CREATE TABLE `fyp-mimic.fyp_extracts.d_icd_diagnoses` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.d_icd_diagnoses`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.kdigo_creatinine`;
CREATE TABLE `fyp-mimic.fyp_extracts.kdigo_creatinine` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.kdigo_creatinine`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.kdigo_uo`;
CREATE TABLE `fyp-mimic.fyp_extracts.kdigo_uo` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.kdigo_uo`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.icustays`;
CREATE TABLE `fyp-mimic.fyp_extracts.icustays` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.icustays`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.inputevents_cv`;
CREATE TABLE `fyp-mimic.fyp_extracts.inputevents_cv` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.inputevents_cv`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.inputevents_mv`;
CREATE TABLE `fyp-mimic.fyp_extracts.inputevents_mv` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.inputevents_mv`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.chartevents`;
CREATE TABLE `fyp-mimic.fyp_extracts.chartevents` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.chartevents`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.icustays`;
CREATE TABLE `fyp-mimic.fyp_extracts.icustays` AS (
    SELECT * FROM `physionet-data.mimiciii_clinical.icustays`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.icustay_detail`;
CREATE TABLE `fyp-mimic.fyp_extracts.icustay_detail` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.icustay_detail`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.oasis`;
CREATE TABLE `fyp-mimic.fyp_extracts.oasis` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.oasis`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.sofa`;
CREATE TABLE `fyp-mimic.fyp_extracts.sofa` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.sofa`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.heightweight`;
CREATE TABLE `fyp-mimic.fyp_extracts.heightweight` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.heightweight`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.elixhauser_quan`;
CREATE TABLE `fyp-mimic.fyp_extracts.elixhauser_quan` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.elixhauser_quan`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.elixhauser_quan_score`;
CREATE TABLE `fyp-mimic.fyp_extracts.elixhauser_quan_score` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.elixhauser_quan_score`
);

DROP TABLE IF EXISTS `fyp-mimic.fyp_extracts.vasopressordurations`;
CREATE TABLE `fyp-mimic.fyp_extracts.vasopressordurations` AS (
    SELECT * FROM `physionet-data.mimiciii_derived.vasopressordurations`
);