-- This script retrieves information about each ICU stay.

SELECT --*
        icustay_id AS ICUSTAY_ID,
        hadm_id AS HADM_ID,
        gender AS GENDER,
        los_hospital AS HOSPITAL_LENGTH_OF_STAY,
        admission_age AS AGE,
        ethnicity_grouped AS ETHNICITY,
        hospital_expire_flag AS HOSPITAL_EXPIRE_FLAG,
        los_icu AS ICU_LENGTH_OF_STAY,
        icustay_seq AS ICU_STAY_SEQUENCE_NUMBER,
        first_icu_stay AS FIRST_ICU_STAY_FLAG
    FROM `fyp-mimic.fyp_extracts.icustay_detail`