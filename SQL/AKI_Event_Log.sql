-- Combine all of the intermediary tables into the event log.

SELECT *
        -- map.HADM_ID AS HADM_ID,
        -- elog.ACTIVITY AS ACTIVITY,
        -- elog.EVENT_TIMESTAMP AS EVENT_TIMESTAMP
    FROM (
        SELECT 
                icustay_id AS ICUSTAY_ID,
                charttime AS EVENT_TIMESTAMP,
                "Colloid bolus administered" AS ACTIVITY
            FROM `fyp-mimic.aki_model.colloid_bolus`

        UNION ALL

        SELECT 
                icustay_id AS ICUSTAY_ID,
                charttime AS EVENT_TIMESTAMP,
                "Crystalloid bolus administered" AS ACTIVITY
            FROM `fyp-mimic.aki_model.crystalloid_bolus`

        UNION ALL

        SELECT 
                icustay_id AS ICUSTAY_ID,
                charttime AS EVENT_TIMESTAMP,
                "Red blood cell transfusion administered" AS ACTIVITY
            FROM `fyp-mimic.aki_model.rbc_transfusions`

        UNION ALL

        SELECT 
                icustay_id AS ICUSTAY_ID,
                EXTRACT(DATETIME FROM starttime) AS EVENT_TIMESTAMP,
                -- starttime AS EVENT_TIMESTAMP,
                "Vasopressor administered" AS ACTIVITY
            FROM `fyp-mimic.fyp_extracts.vasopressordurations` vd

        UNION ALL

         SELECT 
                icustay_ID AS ICUSTAY_ID,
                charttime AS EVENT_TIMESTAMP,
                CONCAT("Tested as KDIGO AKI stage ", aki_stage) AS ACTIVITY,
            FROM `fyp-mimic.aki_model.aki_stages`

        UNION ALL

        SELECT 
                DISTINCT ICUSTAY_ID,
                ICU_IN_TIME AS EVENT_TIMESTAMP,
                "ICU in" AS ACTIVITY
            FROM `fyp-mimic.aki_model.icu_key_events`
            WHERE ICU_IN_TIME IS NOT NULL

        UNION ALL 

        SELECT 
                DISTINCT ICUSTAY_ID,
                ICU_OUT_TIME AS EVENT_TIMESTAMP,
                "ICU out" AS ACTIVITY
            FROM `fyp-mimic.aki_model.icu_key_events`
            WHERE ICU_OUT_TIME IS NOT NULL

        UNION ALL 

        SELECT 
                DISTINCT ICUSTAY_ID,
                DEATH_TIME AS EVENT_TIMESTAMP,
                "Death" AS ACTIVITY
            FROM `fyp-mimic.aki_model.icu_key_events`
            WHERE DEATH_TIME IS NOT NULL

    )
    WHERE ICUSTAY_ID IS NOT NULL
    ORDER BY ICUSTAY_ID, EVENT_TIMESTAMP