-- This script combines all of the intermediary tables and produces a single 'Cases' table

SELECT 
        icustay_details.ICUSTAY_ID,
        icustay_details.HADM_ID,
        icustay_details.GENDER,
        icustay_details.HOSPITAL_LENGTH_OF_STAY,
        icustay_details.AGE,
        CASE 
            WHEN icustay_details.AGE < 18 THEN "Child" 
            WHEN icustay_details.AGE < 55 THEN "Adult Under 55"
            WHEN icustay_details.AGE >= 55 THEN "Adult Over 55"
            ELSE "Unknown"
        END AS AGE_CATEGORY,
        icustay_details.ETHNICITY,
        icustay_details.HOSPITAL_EXPIRE_FLAG,
        icustay_details.ICU_LENGTH_OF_STAY,
        icustay_details.ICU_STAY_SEQUENCE_NUMBER,
        icustay_details.FIRST_ICU_STAY_FLAG,
        oasis.ICUSTAY_EXPIRE_FLAG,
        oasis.OASIS_SCORE,
        oasis.OASIS_PROB_OF_IN_HOSP_MORTALITY,
        sofa.SOFA_RENAL_SCORE,
        bmi.WEIGHT,
        bmi.HEIGHT,
        bmi.BMI,
        bmi.BMI_CATEGORY,
        elix.EXLIXHAUSER_VANWALRAVEN_SCORE,
        elix.EXLIXHAUSER_SID29_SCORE,
        elix.EXLIXHAUSER_SID30_SCORE,
        diagnosis_group.L1_CAT AS DIAGNOSIS_GROUP,
        CASE WHEN EXISTS (
            SELECT *
                FROM `fyp-mimic.fyp_extracts.diagnoses_icd` dicd
                WHERE dicd.ICD9_CODE LIKE "584%" AND dicd.HADM_ID=icustay_details.HADM_ID
        ) THEN 1 ELSE 0 END AS AKI_FLAG,
        CASE WHEN EXISTS (
            SELECT *
                FROM `fyp-mimic.fyp_extracts.diagnoses_icd` dicd
                WHERE dicd.ICD9_CODE LIKE "584%" AND dicd.SEQ_NUM = 1 AND dicd.HADM_ID=icustay_details.HADM_ID
        ) THEN 1 ELSE 0 END AS AKI_PRIMARY_FLAG
    FROM `fyp-mimic.aki_model.icustay_details` icustay_details
    LEFT JOIN `fyp-mimic.aki_model.oasis_extraction` oasis ON icustay_details.ICUSTAY_ID=oasis.ICUSTAY_ID
    LEFT JOIN `fyp-mimic.aki_model.sofa_renal_extraction` sofa ON icustay_details.ICUSTAY_ID=sofa.ICUSTAY_ID
    LEFT JOIN `fyp-mimic.aki_model.bmi_extraction` bmi ON icustay_details.ICUSTAY_ID=bmi.ICUSTAY_ID
    LEFT JOIN `fyp-mimic.aki_model.elixhauser_scores_extraction` elix ON icustay_details.HADM_ID=elix.HADM_ID
    LEFT JOIN `fyp-mimic.aki_model.diagnosis_group` diagnosis_group ON icustay_details.HADM_ID=diagnosis_group.HADM_ID