-- This script extracts some information about key events for each ICU stay

SELECT 
        isd.icustay_id AS ICUSTAY_ID,
        isd.intime AS ICU_IN_TIME,
        isd.outtime AS ICU_OUT_TIME,
        oasis.icustay_expire_flag AS ICUSTAY_EXPIRE_FLAG,
        admissions.DEATHTIME AS DEATH_TIME
    FROM `fyp-mimic.fyp_extracts.icustay_detail` isd
    LEFT JOIN `fyp-mimic.fyp_extracts.oasis` oasis ON isd.icustay_id = oasis.icustay_id
    LEFT JOIN `fyp-mimic.fyp_extracts.admissions` admissions ON isd.hadm_id = admissions.HADM_ID